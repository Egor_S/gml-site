from pelican import signals
import re


def add_youtube_videos(data_passed_from_pelican):
    if data_passed_from_pelican._content:
        full_content_of_page_or_post = data_passed_from_pelican._content
    else:
        return

    all_instances_of_videos = re.findall(r'(\{%\s+youtube\s+[\S]+(?:\s+\[[\d%]+\]\s+\[[\d%]+\])?\s+%\})', full_content_of_page_or_post, re.DOTALL)
    if(len(all_instances_of_videos) > 0):
        updated_full_content_of_page_or_post = full_content_of_page_or_post
        for pre_element_to_parse in all_instances_of_videos:
            replacement_text, subs_cnt = re.subn(r'\{%\s+youtube\s+([\S]+)\s+\[([\d%]+)\]\s+\[([\d%]+)\]\s+%\}',
                                                 r'<div style="text-align: center;">'
                                                 r'<iframe allowfullscreen="" frameborder="0" mozallowfullscreen="" src="https://www.youtube.com/embed/\1" webkitallowfullscreen="" width="\2" height="\3">'
                                                 r'</iframe></div>',
                                                 pre_element_to_parse)
            if subs_cnt == 0:
                replacement_text = re.sub(r'\{%\s+youtube\s+([\S]+)\s+%\}',
                                          r'<div style="text-align: center;">'
                                          r'<iframe allowfullscreen="" frameborder="0" mozallowfullscreen="" src="https://www.youtube.com/embed/\1" webkitallowfullscreen="" width="640" height="360">'
                                          r'</iframe></div>',
                                          pre_element_to_parse)
            updated_full_content_of_page_or_post = \
                updated_full_content_of_page_or_post.replace(pre_element_to_parse, replacement_text)
        data_passed_from_pelican._content = updated_full_content_of_page_or_post


def register():
    signals.content_object_init.connect(add_youtube_videos)
