# Сайт лаборатории компьютерной графики и мультимедиа ВМК МГУ

[Сайт](https://graphics.cs.msu.ru)

Запуск сайта локально по адресу [127.0.0.1:8000](http://127.0.0.1:8000):
```
make localserve
```

Документация:

[О сайте](./docs/about.md)

[Как писать на markdown](./docs/how_markdown.md), [Тестовая страница сайта](https://msu-gml.gitlab.io/gml-site/pages/test_page.html)

[Как добавить новость](./docs/add_news.md)

[Как добавить человека](./docs/add_person.md)

[Как добавить проект](./docs/add_project.md)

[Как добавить курс/спецкурс](./docs/add_course.md)

[Как обновить публикации](./docs/update_publications.md)

[Как отредактировать домашнюю страницу и how to join](./docs/update_static_pages.md)

[Список людей и их Id](./docs/people_list.md)

[Список проектов и их Id](./docs/projects_list.md)

[Как обновить версии библиотек](./docs/update_libs.md)
