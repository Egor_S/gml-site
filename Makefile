PY?=python3
PELICAN?=pelican
PELICANOPTS=

BASEDIR=$(CURDIR)
INPUTDIR=$(BASEDIR)/content
OUTPUTDIR=$(BASEDIR)/public
THEMEDIR=$(BASEDIR)/theme
PUBLISHCONF=$(BASEDIR)/pelicanconf.py
PORT=8000

clean:
	rm -rf "$(OUTPUTDIR)"

download_mathjax:
	if [ ! -d "mathjax" ]; then \
		git clone https://github.com/mathjax/MathJax.git mathjax -b 2.7.3 --depth=1; \
	fi
	cp mathjax $(OUTPUTDIR)/mathjax -r

publish:
	"$(PELICAN)" "$(INPUTDIR)" -o "$(OUTPUTDIR)" -s "$(PUBLISHCONF)" -t "$(THEMEDIR)" $(PELICANOPTS)
	"$(PY)" plugins/check_unused_images.py
	cp $(OUTPUTDIR)/home.html $(OUTPUTDIR)/index.html

localserve: clean publish download_mathjax
	pelican --listen -p $(PORT)

.PHONY: clean publish download_mathjax localserve
