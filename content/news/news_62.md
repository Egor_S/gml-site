Active: yes
Title: Фестиваль Науки 2012
Category: news
Date: 2012-11-06 00:00

![Фестиваль Науки 2012]({static}/images/news/festival_nauki_2012_0.jpg "Фестиваль Науки 2012")

В ходе выставки была проведена демонстрация практических приложений ряда разработанных нами алгоритмов:

- Методов автоматического и полуавтоматического повышения качества 3D-видео (коррекции цветовых и геометрических несоответствий), позволяющих в автоматическом режиме улучшать уже снятое 3D-видео

- Высококачественных автоматических и полуавтоматических алгоритмов конвертации 2D видео в 3D, необходимых киностудиям и кинорежиссерам для работы с современными фильмами:

	* Алгоритмов матирования для обработки сцен с полупрозрачными объектами и границами, позволяющих устранять искажения на краях объектов с нерезкими или очень сложными границами
	* Алгоритмов статического и динамического восстановления фона, позволяющих производить построение виртуальных ракурсов при практически любом значении параллакса, что позволяет получать качественное 3D-видео для автостереоскопических средств воспроизведения
	* Алгоритмов автоматической коррекции и уточнения карты глубины, позволяющих существенно сократить долю ручного труда в процессе создания 3D-видео

- Алгоритмов матирования для обработки сцен с полупрозрачными объектами и границами, позволяющих устранять искажения на краях объектов с нерезкими или очень сложными границами

- Алгоритмов статического и динамического восстановления фона, позволяющих производить построение виртуальных ракурсов при практически любом значении параллакса, что позволяет получать качественное 3D-видео для автостереоскопических средств воспроизведения

- Алгоритмов автоматической коррекции и уточнения карты глубины, позволяющих существенно сократить долю ручного труда в процессе создания 3D-видео

- Алгоритмов построения виртуальных ракурсов в реальном времени, позволяющих «на лету» адаптировать 3D-видео к любым трехмерным средствам воспроизведения вне зависимости от их типа,  структуры и физических параметров

Настоящей звездой выставки стала практическая демонстрация работы наших алгоритмов. Посетителей снимала пара камер, алгоритм в реальном времени на основе двух изображений определял взаимное расположение объектов в кадре и восстанавливал недостающие 26 ракурсов для показа на трехмерном автостереоскопическом мониторе, не требующем очков.

![Фестиваль Науки 2012]({static}/images/news/festival_nauki_2012_1.JPG "Фестиваль Науки 2012")

Не остались равнодушными ни дети, ни взрослые:

![Фестиваль Науки 2012]({static}/images/news/festival_nauki_2012_2.JPG "Фестиваль Науки 2012")

![Фестиваль Науки 2012]({static}/images/news/festival_nauki_2012_3.JPG "Фестиваль Науки 2012")

За проделанную работу сотрудникам лаборатории объявлена благодарность по факультету, особо отличившиеся участники выставки награждены почетными грамотами.

![Фестиваль Науки 2012]({static}/images/news/festival_nauki_2012_4.jpg "Фестиваль Науки 2012")
