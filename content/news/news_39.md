Active: yes
Title: Summerschool submission is over
Category: news
Date: 2011-06-02 00:00

The submission is now over! Competition was particularly stiff this year. The number of registrations at the school website exceeded 500 and the overall acceptance rate was less than 20%. Many of the applications were very strong and the decision was particularly difficult. 82 students have been selected for participation in the school, representing 32 cities from Russia, Ukraine and Belarus and 48 academic institutions and companies.
