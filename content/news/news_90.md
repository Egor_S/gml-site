Active: yes
Title: Our condolenses to parents of Mikhail Sindeyev
Category: news
Date: 2014-02-18 00:00

Mikhail Sindeyev, researcher of our lab, has died February 14, 2014 from stroke at 27. He was one of the most talented graduates of our laboratory and active participant of many research projects. In May 2013 he successfully defended his PhD thesis "Research and developement of video matting algorithms". Our sincere condolenses to his parents. We will deeply miss him.
