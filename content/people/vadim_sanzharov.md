Active: yes
Category: people
PersonType: staff
Id: vadim_sanzharov
Name: Vadim
Surname: Sanzharov
PublicationsPossibleNames: Вадим
PublicationsPossibleSurnames: Санжаров
ParseNewPublications: yes
LabGroups: computer_graphics_group
PersonOrder: 30
Position: Junior researcher
Email: vadim.sanzharov@graphics.cs.msu.ru
IstinaPage:
Photo: images/people/vadim_sanzharov.jpg
ResearchInterests: computer graphics
Projects:

Part of [Hydra](http://www.raytracing.ru/) render development team.
